import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { User } from './user.model';
import { CreateUserDto } from './dto/create-user.dto';

export interface AuthorsData {
    id: number;
    userName: string;
    postsCount: number;
}
@Injectable()
export class UserService {
    constructor(@InjectModel(User) private userRepository: typeof User) { }

    async create(dto: CreateUserDto) {
        const user = await this.userRepository.create(dto);
        return user;
    }
    async getUserById(id: number) {
        const user = await this.userRepository.findOne({ where: { id }, attributes: ["userName"] });
        return user;
    }
    async getUserByName(name: string) {
        const user = await this.userRepository.findOne({ where: { userName: name } });
        return user;
    }
    async getUserByEmail(email: string) {
        const user = await this.userRepository.findOne({ where: { email } });
        return user;
    }
}
