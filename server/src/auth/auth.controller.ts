import { Body, Controller, Post, UseGuards, Get, Req } from '@nestjs/common';
import { CreateUserDto } from 'src/user/dto/create-user.dto';
import { AuthService } from './auth.service';
import { Login } from './dto/login.dto';
import { JwtAuthGuard } from './jwt-auth.guard';
import { TokenResponseDto } from './dto/token.dto';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) { }

    @Post('/login')
    login(@Body() userDto: Login) {
        return this.authService.login(userDto);
    }

    @Post('/registration')
    registration(@Body() userDto: CreateUserDto) {
        return this.authService.registration(userDto);
    }

    @UseGuards(JwtAuthGuard)
    @Get('/check')
    check(@Req() req: TokenResponseDto) {
        return this.authService.checkToken(req);
    }
}
