import { Injectable, HttpException, HttpStatus, UnauthorizedException } from '@nestjs/common';
import { UserService } from 'src/user/user.service';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from 'src/user/dto/create-user.dto';
import * as bcrypt from 'bcryptjs';
import { User } from 'src/user/user.model';
import { Login } from './dto/login.dto';

@Injectable()
export class AuthService {
    constructor(private userService: UserService, private jwtService: JwtService) { }

    async login(userDto: Login) {
        const user = await this.validateRegularUser(userDto);
        return this.generateToken(user);
    }


    async registration(userDto: CreateUserDto) {
        const candidateEmail = await this.userService.getUserByEmail(userDto.email);
        const candidateName = await this.userService.getUserByName(userDto.userName);
        if (candidateEmail || candidateName) {
            throw new HttpException({ message: 'User with this email or name already exists' }, HttpStatus.BAD_REQUEST);
        }
        const hashPassword = await bcrypt.hash(userDto.password, 5);
        const user = await this.userService.create({ ...userDto, password: hashPassword });
        return this.generateToken(user);
    }

    private generateToken(user: User) {
        const payload = { email: user.email, id: user.id, userName: user.userName };
        return {
            token: this.jwtService.sign(payload)
        };
    }

    private async validateRegularUser(userDto: Login) {
        const user = await this.userService.getUserByEmail(userDto.email);
        if (!user) {
            throw new UnauthorizedException({ message: "User doesn't exist" });
        }
        const passwordEquals = await bcrypt.compare(userDto.password, user.password);
        if (user && passwordEquals) {
            return user;
        }
        throw new UnauthorizedException({ message: 'Wrong password or email' });
    }

    checkToken(req: any) {
        const token = this.generateToken(req.user);
        return token;
    }
}
