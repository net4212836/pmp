import { Injectable } from '@nestjs/common';
import { HistoryDto } from './dto/history.dto';
import { InjectModel } from '@nestjs/sequelize';
import { History } from './history.model';

@Injectable()
export class HistoryService {
    constructor(@InjectModel(History) private historyRepository: typeof History
    ) { }
    async create(historyDto: HistoryDto) {
        const history = await this.historyRepository.create(historyDto);
        return history;
    }

    async getAllUserHistory(id: number) {
        const userHistory = await this.historyRepository.findAll({ where: { userId: id } });
        const sortedHistory = userHistory.sort((a, b) => b.id - a.id)
        return sortedHistory;
    }

    delete(id: number) {
        return History.destroy({ where: { id } })
    }
}
