import { IsNumber, IsNumberString, IsString } from 'class-validator';

export class HistoryDto {
    @IsNumber({}, { message: "Must be number" })
    readonly userId: number;
    @IsString({ message: "Must be string" })
    readonly inputCurrency: string;
    @IsString({ message: "Must be string" })
    readonly outputCurrency: string;
    @IsNumber({}, { message: "Must be number" })
    readonly inputAmount: number;
    @IsNumber({}, { message: "Must be number" })
    readonly outputAmount: number;
}