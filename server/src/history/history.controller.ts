import { Controller, Body, Post, UseGuards, Delete, Param, Get } from '@nestjs/common';
import { HistoryService } from './history.service';
import { } from '@nestjs/common';
import { HistoryDto } from './dto/history.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('history')
export class HistoryController {
    constructor(private historyService: HistoryService) { }
    @UseGuards(JwtAuthGuard)
    @Post('/create')
    login(@Body() historyDto: HistoryDto) {
        return this.historyService.create(historyDto);
    }
    @UseGuards(JwtAuthGuard)
    @Delete('/:id')
    registration(@Param('id') id: number) {
        return this.historyService.delete(id);
    }

    @UseGuards(JwtAuthGuard)
    @Get('/:id')
    getAllUserHistory(@Param('id') id: number) {
        return this.historyService.getAllUserHistory(id);
    }
}
