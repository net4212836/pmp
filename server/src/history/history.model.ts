import { Table, DataType, Column, Model, ForeignKey } from "sequelize-typescript";
import { User } from 'src/user/user.model';
interface HistoryCreationAttrs {
    userId: number;
    inputCurrency: string;
    outputCurrency: string;
    inputAmount: number;
    outputAmount: number;
}

@Table({ tableName: 'history' })
export class History extends Model<History, HistoryCreationAttrs> {
    @Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
    id: number;
    @ForeignKey(() => User)
    @Column({ type: DataType.INTEGER })
    userId: number;
    @Column({ type: DataType.STRING, allowNull: false })
    inputCurrency: string;
    @Column({ type: DataType.STRING, allowNull: false })
    outputCurrency: string;
    @Column({ type: DataType.DECIMAL, allowNull: false })
    inputAmount: number;
    @Column({ type: DataType.DECIMAL, allowNull: false })
    outputAmount: number;
}
