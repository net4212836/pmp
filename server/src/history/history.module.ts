import { Module } from '@nestjs/common';
import { HistoryController } from './history.controller';
import { HistoryService } from './history.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { History } from './history.model';
import { User } from 'src/user/user.model';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  controllers: [HistoryController],
  providers: [HistoryService],
  imports: [
    SequelizeModule.forFeature([History, User]), AuthModule
  ]
})
export class HistoryModule { }
