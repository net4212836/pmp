type Currency = {
    id: string;
    name: string;
}

type CurrencyResponse = Currency & {
    min_size: string;
}

type Exchange = {
    currencyName: string;
    exchangedCurrencyName: string;
    amount: number
}

export { type CurrencyResponse, type Currency, type Exchange }