import { NavigationList } from '../constants/navigation-list';

type NavigationParameterList = {
    [NavigationList.Login]: undefined;
    [NavigationList.History]: undefined;
    [NavigationList.Main]: undefined;
    [NavigationList.Register]: undefined;
    [NavigationList.FirstCurrency]: undefined;
    [NavigationList.SecondCurrency]: undefined;
}

export { type NavigationParameterList }