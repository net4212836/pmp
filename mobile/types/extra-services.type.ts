import { Auth } from '../api/auth/auth-api'
import { Exchange } from '../api/exchange/exchange-api'
import { History } from '../api/history/history-api'

type ThunkAuthExtraServices = {
    auth: Auth
}

type ThunkExchangeExtraServices = {
    exchange: Exchange,
    history: History
}

type ThunkHistoryExtraServices = {
    history: History
}



export { type ThunkAuthExtraServices, type ThunkExchangeExtraServices, type ThunkHistoryExtraServices }