type CreateHistory = {
    userId: number;
    inputCurrency: string;
    outputCurrency: string;
    inputAmount: number;
    outputAmount: number;
}

type History = CreateHistory & {
    id: number;
    createdAt: string;
    updatedAt: string;
}

export { type CreateHistory, type History }