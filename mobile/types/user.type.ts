import { Currency } from './exchange.type';
import { History } from './history.type';

type User = {
    userName: string;
    email: string;
    id: number;
}

type UserState = {
    user: UserSlice
}

type UserSlice = {
    user: User | null;
    firstCurrency: Currency | null;
    secondCurrency: Currency | null;
    exchangedAmount: number;
    userHistory: History[]
}

export { type UserSlice, type User, type UserState }