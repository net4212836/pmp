type Login = {
    email: string;
    password: string;
}

type Register = {
    email: string;
    password: string;
    userName: string;
}

export { type Login, type Register }