import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { setupStore } from './store/store';
import { Provider } from 'react-redux';
import { Navigate } from './components/navigate/navigate';

export default function App() {
  const store = setupStore();

  return (
    <NavigationContainer>
      <Provider store={store}>
        <Navigate />
      </Provider>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
