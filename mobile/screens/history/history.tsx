import React from 'react';
import { useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import { useAppDispatch, useAppSelector } from '../../hooks/reduxHooks';
import { actions as userActions } from '../../slices/user/user';
import { HistoryElement } from './components/history-element';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { NavigationParameterList } from '../../types/navigation-list.type';
import { NavigationList } from '../../constants/navigation-list';

const History: React.FC = () => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(userActions.loadHistory());
  }, []);
  const navigation = useNavigation<NavigationProp<NavigationParameterList>>();
  const histories = useAppSelector((state) => state.user.userHistory);
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity
          style={styles.backBtn}
          onPress={() => navigation.navigate(NavigationList.Main)}
        >
          <Text style={styles.backBtnText}>Back</Text>
        </TouchableOpacity>
        <Text style={styles.greeting}>Welcome to history</Text>
      </View>
      <ScrollView style={styles.scrollableContainer}>
        <View style={styles.scrollContainer}>
          {histories?.map((history, index) => (
            <HistoryElement
              history={history}
              key={history.id}
              isLast={index === histories.length - 1}
            />
          ))}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: 'black',
    paddingTop: 50,
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    gap: 10,
  },
  greeting: {
    color: 'white',
    fontSize: 24,
  },
  scrollableContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: 'black',
  },
  backBtn: {
    width: '20%',
    height: 50,
    backgroundColor: '#31C4DB',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  backBtnText: {
    color: 'white',
    fontSize: 24,
  },
  scrollContainer: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-start',
    width: '100%',
    height: '100%',
    alignItems: 'center',
    backgroundColor: 'black',
    flexDirection: 'column',
  },
});

export { History };
