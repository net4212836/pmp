import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { History } from '../../../types/history.type';
import { actions as userActions } from '../../../slices/user/user';
import { useAppDispatch } from '../../../hooks/reduxHooks';

type Properties = {
  history: History;
  isLast: boolean;
};

const HistoryElement: React.FC<Properties> = ({ history, isLast }) => {
  const dispatch = useAppDispatch();
  const event = new Date(history.createdAt).toISOString();
  const date = event.split('T')[0];
  const time = event.split('T')[1].split('.')[0];
  const handleDelete = () => {
    dispatch(userActions.deleteHistory(history.id));
  };
  return (
    <View
      style={[
        styles.container,
        isLast ? styles.lastContainer : styles.regularContainer,
      ]}
    >
      <View>
        <View>
          <Text style={styles.textInfo}>
            Input currency: {history.inputCurrency}
          </Text>
          <Text style={styles.textInfo}>
            Output currency: {history.outputCurrency}
          </Text>
        </View>
        <View>
          <Text style={styles.textInfo}>
            Input amount: {history.inputAmount}
          </Text>
          <Text style={styles.textInfo}>
            Output amount: {history.outputAmount}
          </Text>
        </View>
        <Text style={styles.textInfo}>
          Date: {date}, {time}
        </Text>
      </View>
      <TouchableOpacity style={styles.delete} onPress={handleDelete}>
        <Text style={styles.deleteText}>Delete</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#373C38',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
  },
  regularContainer: {
    borderTopColor: 'white',
    borderTopWidth: 1,
  },
  lastContainer: {
    borderBottomColor: 'white',
    borderBottomWidth: 1,
    borderTopColor: 'white',
    borderTopWidth: 1,
  },
  delete: {
    width: 60,
    borderRadius: 10,
    height: 30,
    backgroundColor: 'red',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  deleteText: {
    color: 'white',
    fontSize: 16,
  },
  textInfo: {
    fontSize: 16,
    color: 'white',
  },
});

export { HistoryElement };
