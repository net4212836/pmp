import React from 'react';
import { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
  ToastAndroid,
} from 'react-native';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { NavigationParameterList } from '../../types/navigation-list.type';
import { NavigationList } from '../../constants/navigation-list';
import { useAppDispatch, useAppSelector } from '../../hooks/reduxHooks';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { actions as userActions } from '../../slices/user/user';
import * as Clipboard from 'expo-clipboard';

const Main: React.FC = () => {
  const { user, firstCurrency, secondCurrency, exchangedAmount } =
    useAppSelector((state) => state.user);
  const dispatch = useAppDispatch();
  const [amount, setAmount] = useState(0);

  const handleExchange = () => {
    if (!firstCurrency || !secondCurrency) {
      ToastAndroid.show('Choose both currencies', ToastAndroid.SHORT);
      return;
    }
    dispatch(
      userActions.exchange({
        currencyName: firstCurrency?.id as string,
        exchangedCurrencyName: secondCurrency?.id as string,
        amount,
      })
    );
  };

  const navigation = useNavigation<NavigationProp<NavigationParameterList>>();
  const handleLogout = async () => {
    await AsyncStorage.removeItem('token').then(() => {
      navigation.navigate(NavigationList.Login);
      dispatch(userActions.removeUser());
    });
  };
  const handleCopy = async () => {
    await Clipboard.setStringAsync(`${exchangedAmount}`);
  };
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.greeting}>CurrencySage is glad to serve you</Text>
      <View style={styles.currencyContainer}>
        <View style={styles.selectCurrencyContainer}>
          <TouchableOpacity
            style={styles.selectCurrencyBtn}
            onPress={() => navigation.navigate(NavigationList.FirstCurrency)}
          >
            <Text style={styles.selectCurrencyBtnText}>Input currency</Text>
          </TouchableOpacity>
          <Text style={styles.currencyName}>
            {firstCurrency ? firstCurrency.name : <>N/A</>}
          </Text>
        </View>
        <View style={styles.selectCurrencyContainer}>
          <TouchableOpacity
            style={styles.selectCurrencyBtn}
            onPress={() => navigation.navigate(NavigationList.SecondCurrency)}
          >
            <Text style={styles.selectCurrencyBtnText}>Output currency</Text>
          </TouchableOpacity>
          <Text style={styles.currencyName}>
            {secondCurrency ? secondCurrency.name : <>N/A</>}
          </Text>
        </View>
      </View>
      <TextInput
        placeholderTextColor={'#868E88'}
        placeholder='Enter amount'
        style={styles.inputField}
        keyboardType='numeric'
        value={String(amount)}
        onChangeText={(text) => setAmount(Number(text))}
      />
      <TouchableOpacity style={styles.submit} onPress={handleExchange}>
        <Text style={styles.submitText}>Exchange</Text>
      </TouchableOpacity>
      <View style={styles.exchangeAmountContainer}>
        <Text style={styles.exchangedAmount}>
          Exchanged amount: {exchangedAmount}
        </Text>
        <TouchableOpacity style={styles.copyBtn} onPress={handleCopy}>
          <Text style={styles.submitText}>Copy</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.navContainer}>
        <TouchableOpacity
          style={styles.logout}
          onPress={
            user
              ? handleLogout
              : () => navigation.navigate(NavigationList.Login)
          }
        >
          <Text style={styles.selectCurrencyBtnText}>
            {user ? <>Logout</> : <>Enter account</>}
          </Text>
        </TouchableOpacity>
        {user && (
          <TouchableOpacity
            style={styles.openHistory}
            onPress={() => navigation.navigate(NavigationList.History)}
          >
            <Text style={styles.selectCurrencyBtnText}>Open history</Text>
          </TouchableOpacity>
        )}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: 'black',
  },
  container: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    padding: 20,
    gap: 40,
  },
  greeting: {
    fontSize: 30,
    fontWeight: '600',
    color: 'green',
  },
  inputField: {
    width: '100%',
    height: 50,
    backgroundColor: '#373C38',
    fontSize: 24,
    padding: 10,
    color: 'white',
    textDecorationLine: 'none',
  },
  submit: {
    width: '100%',
    height: 50,
    backgroundColor: '#31C4DB',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  submitText: {
    color: 'white',
    fontSize: 24,
  },
  currencyContainer: {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  selectCurrencyContainer: {
    display: 'flex',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  selectCurrencyBtn: {
    width: 180,
    borderRadius: 10,
    height: 50,
    backgroundColor: 'green',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  selectCurrencyBtnText: {
    color: 'white',
    fontSize: 20,
  },
  currencyName: {
    paddingTop: 10,
    textAlign: 'center',
    color: 'white',
    fontSize: 18,
    width: '100%',
  },
  navContainer: {
    paddingTop: 10,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    gap: 10,
  },
  logout: {
    width: 180,
    borderRadius: 10,
    height: 50,
    backgroundColor: 'red',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  openHistory: {
    width: 180,
    borderRadius: 10,
    height: 50,
    backgroundColor: '#868E88',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  exchangeAmountContainer: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    gap: 20,
  },
  exchangedAmount: {
    color: 'white',
    fontSize: 24,
    textAlign: 'center',
  },
  copyBtn: {
    width: '100%',
    backgroundColor: 'green',
    height: 50,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
});

export { Main };
