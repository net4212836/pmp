import React from 'react';
import { useState } from 'react';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import { useAppDispatch } from '../../hooks/reduxHooks';
import { NavigationList } from '../../constants/navigation-list';
import { NavigationParameterList } from '../../types/navigation-list.type';
import { actions as authActions } from '../../slices/user/user';

const Register: React.FC = () => {
  const [userData, setUserData] = useState({
    email: '',
    password: '',
    userName: '',
  });
  const dispatch = useAppDispatch();
  const navigation = useNavigation<NavigationProp<NavigationParameterList>>();
  const handleRegister = () => {
    dispatch(authActions.register(userData))
      .unwrap()
      .then(() => navigation.navigate(NavigationList.Main));
  };

  return (
    <View style={styles.container}>
      <Text style={styles.greeting}>Welcome to CurrencySage</Text>
      <TextInput
        style={styles.inputField}
        placeholderTextColor={'#868E88'}
        placeholder='Enter name'
        value={userData.userName}
        onChangeText={(text) =>
          setUserData((previous) => {
            return { ...previous, userName: text };
          })
        }
      />
      <TextInput
        style={styles.inputField}
        placeholderTextColor={'#868E88'}
        placeholder='Enter e-mail'
        value={userData.email}
        onChangeText={(text) =>
          setUserData((previous) => {
            return { ...previous, email: text };
          })
        }
      />
      <TextInput
        style={styles.inputField}
        placeholderTextColor={'#868E88'}
        placeholder='Enter password'
        secureTextEntry={true}
        value={userData.password}
        onChangeText={(text) =>
          setUserData((previous) => {
            return { ...previous, password: text };
          })
        }
      />
      <TouchableOpacity style={styles.login} onPress={handleRegister}>
        <Text style={styles.loginText}>Register</Text>
      </TouchableOpacity>
      <View style={styles.linkContainer}>
        <TouchableOpacity
          onPress={() => navigation.navigate(NavigationList.Main)}
        >
          <Text style={styles.linkText}>Continue without account</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate(NavigationList.Login)}
        >
          <Text style={styles.linkText}>Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    padding: 20,
    gap: 20,
  },
  greeting: {
    fontSize: 30,
    fontWeight: '600',
    color: 'green',
  },
  inputField: {
    width: '100%',
    height: 50,
    backgroundColor: '#373C38',
    fontSize: 24,
    padding: 10,
    color: 'white',
    textDecorationLine: 'none',
  },
  login: {
    width: '100%',
    height: 50,
    backgroundColor: '#31C4DB',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginText: {
    color: 'white',
    fontSize: 24,
  },
  linkContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
  },
  linkText: {
    color: 'white',
    fontSize: 16,
  },
});

export { Register };
