import React from 'react';
import { useEffect, useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { NavigationParameterList } from '../../types/navigation-list.type';
import { NavigationList } from '../../constants/navigation-list';
import { CurrencyResponse } from '../../types/exchange.type';
import { exchange } from '../../api/exchange/exchange';
import { useSearch } from '../../hooks/useSearch';
import { useAppDispatch } from '../../hooks/reduxHooks';
import { actions as userActions } from '../../slices/user/user';

const SecondCurrency: React.FC = () => {
  const [currency, setCurrency] = useState<CurrencyResponse[]>([]);
  const [loading, setLoading] = useState(true);
  const [query, setQuery] = useState('');
  const dispatch = useAppDispatch();
  const loadCurrency = async () => {
    setCurrency(await exchange.getAllCurrency());
    setLoading(false);
  };
  useEffect(() => {
    loadCurrency();
  }, []);
  const navigation = useNavigation<NavigationProp<NavigationParameterList>>();
  const searchedItems = useSearch(query, currency);

  const handleSelectCurrency = (id: string, name: string) => {
    dispatch(userActions.setSecondCurrency({ id, name }));
    navigation.navigate(NavigationList.Main);
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <TouchableOpacity
          style={styles.backBtn}
          onPress={() => navigation.navigate(NavigationList.Main)}
        >
          <Text style={styles.backBtnText}>Back</Text>
        </TouchableOpacity>
        <TextInput
          placeholderTextColor={'#868E88'}
          placeholder='Enter currency'
          style={styles.inputField}
          value={query}
          onChangeText={(text) => setQuery(text)}
        />
      </View>

      <ScrollView style={styles.scroll}>
        <View style={styles.scrollContainer}>
          {loading ? (
            <ActivityIndicator size='large' />
          ) : (
            <>
              {searchedItems.map((item) => (
                <TouchableOpacity
                  key={item.id}
                  style={{
                    display: 'flex',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    width: '100%',
                    padding: 10,
                    borderBottomColor: 'white',
                    borderBottomWidth: 1,
                  }}
                  onPress={() => handleSelectCurrency(item.id, item.name)}
                >
                  <Text style={{ color: 'white', fontSize: 24, width: '100%' }}>
                    {item.name}
                  </Text>
                </TouchableOpacity>
              ))}
            </>
          )}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    flexDirection: 'column',
  },
  headerContainer: {
    display: 'flex',
    width: '100%',
    flexDirection: 'row',
  },
  backBtn: {
    width: '20%',
    height: 50,
    backgroundColor: '#31C4DB',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  backBtnText: {
    color: 'white',
    fontSize: 24,
  },
  scroll: {
    flex: 1,
    width: '100%',
    backgroundColor: 'black',
    height: '100%',
  },
  scrollContainer: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    flexDirection: 'column',
  },

  inputField: {
    width: '100%',
    height: 50,
    backgroundColor: '#373C38',
    fontSize: 24,
    padding: 10,
    color: 'white',
    textDecorationLine: 'none',
  },
});

export { SecondCurrency };
