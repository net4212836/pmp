import React from 'react';
import { useEffect, useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationList } from '../../constants/navigation-list';
import { Main } from '../../screens/main/main';
import { FirstCurrency } from '../../screens/first-curency/first-currency';
import { SecondCurrency } from '../../screens/second-currency/second-currency';
import { History } from '../../screens/history/history';
import { Login } from '../../screens/login/login';
import { Register } from '../../screens/register/register';
import { useAppDispatch, useAppSelector } from '../../hooks/reduxHooks';
import { actions as userActions } from '../../slices/user/user';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationParameterList } from '../../types/navigation-list.type';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { ActivityIndicator, View, StyleSheet } from 'react-native';

const Stack = createStackNavigator();

const Navigate: React.FC = () => {
  const dispatch = useAppDispatch();
  const navigation = useNavigation<NavigationProp<NavigationParameterList>>();
  const [loading, setLoading] = useState(true);
  const checkToken = async () => {
    const token = (await AsyncStorage.getItem('token')) as string;
    if (!token) {
      return setLoading(false);
    }
    dispatch(userActions.checkToken(token))
      .unwrap()
      .then(() => setLoading(false))
      .then(() => navigation.navigate(NavigationList.Main))
      .finally(() => setLoading(false));
  };

  useEffect(() => {
    checkToken();
  }, []);
  const user = useAppSelector((state) => state.user.user);
  return (
    <>
      {loading ? (
        <View style={styles.container}>
          <ActivityIndicator size='large' />
        </View>
      ) : (
        <>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}
          >
            <Stack.Screen name={NavigationList.Login} component={Login} />
            <Stack.Screen name={NavigationList.Register} component={Register} />
            <Stack.Screen name={NavigationList.Main} component={Main} />
            {user && (
              <Stack.Screen name={NavigationList.History} component={History} />
            )}

            <Stack.Screen
              name={NavigationList.FirstCurrency}
              component={FirstCurrency}
            />
            <Stack.Screen
              name={NavigationList.SecondCurrency}
              component={SecondCurrency}
            />
          </Stack.Navigator>
        </>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export { Navigate };
