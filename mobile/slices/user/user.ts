import {
    login,
    register,
    checkToken,
    exchange,
    loadHistory,
    deleteHistory
} from './actions';
import { actions } from './user.slice'
const allActions = {
    ...actions,
    login,
    register,
    checkToken,
    exchange,
    loadHistory,
    deleteHistory
};
export { allActions as actions };
export { reducer } from './user.slice';