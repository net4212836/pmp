import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';
import { User, UserSlice, UserState } from '../../types/user.type';
import { Login, Register } from '../../types/auth.type';
import { ThunkAuthExtraServices, ThunkExchangeExtraServices, ThunkHistoryExtraServices } from '../../types/extra-services.type';
import { Exchange } from '../../types/exchange.type';
import { History } from '../../types/history.type';

const login = createAsyncThunk<User | undefined, Login, { extra: { services: ThunkAuthExtraServices }, rejectValue: string }>(
    ActionType.LOGIN,
    async (request: Login, { extra: { services }, rejectWithValue }) => {
        const data = await services.auth.login(request);
        if (!data) {
            return rejectWithValue('Error');
        }
        return data;
    }
);

const register = createAsyncThunk<User | undefined, Register, { extra: { services: ThunkAuthExtraServices }, rejectValue: string }>(
    ActionType.REGISTER,
    async (request: Register, { extra: { services }, rejectWithValue }) => {
        const data = await services.auth.register(request);
        if (!data) {
            return rejectWithValue('Error');
        }
        return data;
    }
);

const checkToken = createAsyncThunk<User, string, { extra: { services: ThunkAuthExtraServices }, rejectValue: string }>(
    ActionType.CHECK,
    async (request: string, { extra: { services }, rejectWithValue }) => {
        const data = await services.auth.checkToken();
        if (!data) {
            return rejectWithValue('Error');
        }
        return data;
    }
);

const exchange = createAsyncThunk<{ exchangedAmount: number, history: History }, Exchange, { state: UserState, extra: { services: ThunkExchangeExtraServices }, rejectValue: string }>(
    ActionType.EXCHANGE,
    async (request: Exchange, { getState, extra: { services }, rejectWithValue }) => {
        const exchangedAmount = await services.exchange.getInputCurrencyExchangePrice(request.currencyName, request.exchangedCurrencyName, request.amount);
        const { user: { firstCurrency, secondCurrency, user } } = getState()
        if (!exchangedAmount) {
            return rejectWithValue('Error');
        }
        if (!user) {
            return { exchangedAmount, history: null };
        }
        const history = await services.history.createHistory({ userId: user.id, inputCurrency: firstCurrency?.name as string, outputCurrency: secondCurrency?.name as string, inputAmount: request.amount, outputAmount: exchangedAmount });
        return { exchangedAmount, history };
    }
);

const loadHistory = createAsyncThunk<History[], void, { state: UserState, extra: { services: ThunkHistoryExtraServices }, rejectValue: string }>(
    ActionType.LOAD_HISTORY,
    async (request: void, { getState, extra: { services }, rejectWithValue }) => {
        const { user: { user } } = getState();
        if (!user) {
            return
        }
        const histories = await services.history.getAllUserHistory(user.id);
        return histories;
    }
);

const deleteHistory = createAsyncThunk<History[], number, { state: UserState, extra: { services: ThunkHistoryExtraServices }, rejectValue: string }>(
    ActionType.REMOVE_HISTORY,
    async (request: number, { getState, extra: { services }, rejectWithValue }) => {
        const { user: { userHistory } } = getState();
        await services.history.removeHistory(request);
        const newHistories = userHistory?.filter((history) => history.id !== request);
        return newHistories;
    }
);

export { login, register, checkToken, exchange, loadHistory, deleteHistory }