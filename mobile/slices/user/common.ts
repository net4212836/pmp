const ActionType = {
    LOGIN: 'user/LOGIN',
    REGISTER: 'user/REGISTER',
    CHECK: 'user/CHECK',
    EXCHANGE: 'user/EXCHANGE',
    LOAD_HISTORY: 'user/LOAD_HISTORY',
    REMOVE_HISTORY: 'user/REMOVE_HISTORY'
};

export { ActionType };