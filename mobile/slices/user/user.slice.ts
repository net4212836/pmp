import { createSlice, isAnyOf } from '@reduxjs/toolkit';
import { User, UserSlice } from '../../types/user.type';
import { checkToken, deleteHistory, exchange, loadHistory, login, register } from './actions';

const initialState: UserSlice = {
    user: null,
    firstCurrency: null,
    secondCurrency: null,
    exchangedAmount: 0,
    userHistory: []
};

const { reducer, actions, name } = createSlice({
    initialState,
    name: 'user',
    reducers: {
        removeUser: (state) => {
            state.user = null;
        },
        setFirstCurrency: (state, action) => {
            state.firstCurrency = action.payload
        },
        setSecondCurrency: (state, action) => {
            state.secondCurrency = action.payload
        }
    },
    extraReducers(builder) {
        builder
            .addMatcher(
                isAnyOf(
                    login.fulfilled,
                    register.fulfilled,
                    checkToken.fulfilled,
                ),
                (state, action) => {
                    state.user = action.payload as User;
                }
            ).addMatcher(
                isAnyOf(
                    login.rejected,
                    register.rejected,
                    checkToken.rejected
                ),
                (state, action) => {
                    state.user = null;
                }
            ).addMatcher(
                isAnyOf(
                    exchange.fulfilled
                ),
                (state, action) => {
                    if (!action.payload.history) {
                        state.exchangedAmount = action.payload.exchangedAmount;
                        state.userHistory = [];
                        return;
                    }
                    state.exchangedAmount = action.payload.exchangedAmount;
                    state.userHistory?.unshift(action.payload.history);
                }
            ).addMatcher(
                isAnyOf(
                    loadHistory.fulfilled,
                    deleteHistory.fulfilled
                ),
                (state, action) => {
                    state.userHistory = action.payload;
                }
            )
            .addMatcher(
                isAnyOf(
                    loadHistory.rejected,
                    deleteHistory.rejected
                ),
                (state, action) => {
                    state.userHistory = [];
                }
            )
    }
});

export { actions, name, reducer };