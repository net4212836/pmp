import { configureStore, combineReducers } from "@reduxjs/toolkit";
import { reducer as userReducer } from "../slices/user/user";
import { auth } from '../api/auth/auth';
import { exchange } from '../api/exchange/exchange';
import { history } from '../api/history/history';


export const rootReducer = combineReducers({
    user: userReducer,
});

export const setupStore = () => {
    return configureStore({
        reducer: rootReducer,
        middleware: getDefaultMiddleware =>
            getDefaultMiddleware({
                thunk: {
                    extraArgument: {
                        services: {
                            auth,
                            exchange,
                            history
                        }
                    }
                }
            }),
    });
}

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = AppStore["dispatch"];
