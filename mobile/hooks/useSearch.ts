import { useMemo } from "react"
import { CurrencyResponse } from '../types/exchange.type';

const useSearch = (query: string, items: CurrencyResponse[]) => {
    const searchedItems = useMemo(() => {
        const re = new RegExp(query.toLowerCase());
        return items.filter((item) => item.name.toLowerCase().search(re) !== -1)
    }, [items, query]);
    return searchedItems;
}

export { useSearch }