const NavigationList = {
    Login: 'Login',
    Register: 'Register',
    Main: 'Main',
    History: 'History',
    FirstCurrency: 'FirstCurrency',
    SecondCurrency: 'SecondCurrency'
} as const;

export { NavigationList }