import { $host } from '..';
import jwt_decode from 'jwt-decode';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Login, Register } from '../../types/auth.type';
import { User } from '../../types/user.type';
import { ToastAndroid } from 'react-native';

export class Auth {
    constructor() { }

    async login(userData: Login) {
        try {
            const { data } = await $host.post('/auth/login', userData);
            await AsyncStorage.setItem('token', data.token);
            return jwt_decode(data.token) as User;
        } catch (error: any) {
            ToastAndroid.show(error.response.data.message, ToastAndroid.SHORT);
        }

    }

    async register(userData: Register) {
        try {
            const { data } = await $host.post('/auth/registration', userData);
            await AsyncStorage.setItem('token', data.token);
            return jwt_decode(data.token) as User;
        } catch (error: any) {
            ToastAndroid.show(error.response.data.message, ToastAndroid.SHORT);
        }
    }

    async checkToken() {
        try {
            const { data } = await $host.get('/auth/check');
            await AsyncStorage.setItem('token', data.token);
            return jwt_decode(data.token) as User;
        } catch (error: any) {
            ToastAndroid.show(error.response.data.message, ToastAndroid.SHORT);
        }
    }
}