import axios from 'axios';
import { $exchange } from '..';

export class Exchange {
    constructor() { }

    async getAllCurrency() {
        const { data } = await $exchange.get('/currencies')
        return data.data;
    }

    async getInputCurrencyExchangePrice(currencyName: string, exchangedCurrencyName: string, amount: number) {
        const { data } = await $exchange.get(`/exchange-rates?currency=${currencyName}`)
        const exchangedCurrencyValue = Number(data.data.rates[exchangedCurrencyName]);
        const exchangedAmount = exchangedCurrencyValue * amount;
        return Number(exchangedAmount.toFixed(2));
    }
}