import { $host } from '..';
import { CreateHistory } from '../../types/history.type';
import { ToastAndroid } from 'react-native';

export class History {
    constructor() { }

    async getAllUserHistory(userId: number) {
        const { data } = await $host.get(`/history/${userId}`);
        return data;
    }

    async removeHistory(id: number) {
        const { data } = await $host.delete(`/history/${id}`);
        return data;
    }

    async createHistory(history: CreateHistory) {
        try {
            const { data } = await $host.post('/history/create', history);
            return data;
        } catch (error: any) {
            console.log(error)
            ToastAndroid.show(error.response.data.message, ToastAndroid.SHORT);
        }

    }
}