import AsyncStorage from '@react-native-async-storage/async-storage';
import axios, { InternalAxiosRequestConfig } from "axios";

const $host = axios.create({
    baseURL: process.env.EXPO_PUBLIC_API_URL
})

const $exchange = axios.create({
    baseURL: process.env.EXPO_PUBLIC_CURRENCY_EXCHANGE_URL_API
})

const authInterceptor = async (config: InternalAxiosRequestConfig<any>) => {
    config.headers.authorization = `Bearer ${await AsyncStorage.getItem('token')}`
    return config
}
$host.interceptors.request.use(authInterceptor)
export {
    $host,
    $exchange
}
